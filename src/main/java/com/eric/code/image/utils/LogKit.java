package com.eric.code.image.utils;

/**
 * Created by eric on 16/8/21.
 */
public class LogKit {
    public static final String TAG = "eric" ;
    public static void log(Object log){
        System.out.println(String.format("%s  -->  %s",TAG,log));
    }
    public static void logf(String format,Object... log){
        System.out.println(TAG+"  --> " + String.format(format,log));
    }
}
