package com.eric.code.image;


import com.eric.code.image.utils.LogKit;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * Created by eric on 16-8-25.
 */
public class ImageKit {
    public static void resizeGifAndExtractLogo(InputStream in, OutputStream out, OutputStream logo, int maxWidth, int maxHeight, float quality,
                                               String[] watermark, Font font, Color fontColor) throws IOException {
        GifDecoder gd = new GifDecoder();
        int status = gd.read(in);
        if (status != GifDecoder.STATUS_OK) {
            return;
        }
        AnimatedGifEncoder ge = new AnimatedGifEncoder();
        ge.start(out);
        ge.setRepeat(0);

        int count = gd.getFrameCount() ;
        int jump = getInt(count) ;
        int sampling = count/2;
        LogKit.logf(">>>>%d>>>>>>%d>>>>>>>",count,jump);
        boolean heightWeight = false ;
        for (int i = 0; i < gd.getFrameCount(); i++) {
            BufferedImage frame = gd.getFrame(i);
            int width = frame.getWidth();
            int height = frame.getHeight();
            if(width < height){
                heightWeight = true ;
            }

            if(i % jump != 0){
                continue;
            }
            // 80%
            width = (int) (width * 0.5);
            height = (int) (height * 0.5);
            //
            BufferedImage rescaled = Scalr.resize(frame, heightWeight?Scalr.Mode.FIT_TO_HEIGHT:Scalr.Mode.FIT_TO_WIDTH, width, height);
            if(i > sampling ){
                ImageIO.write(rescaled,"png",logo);
            }
            //
            int delay = gd.getDelay(i);
            //
LogKit.log(">>>>"+delay);
            ge.setDelay((int)(delay*2)); //
            ge.addFrame(rescaled);
        }

        ge.finish();
    }

    public static void main(String []s) throws IOException{

        GifDecoder gd = new GifDecoder();
        int status = gd.read(new FileInputStream(new File("/home/eric/old.gif")));
        if (status != GifDecoder.STATUS_OK) {
            return;
        }

        AnimatedGifEncoder ge = new AnimatedGifEncoder();
        ge.start(new FileOutputStream(new File("/home/eric/old_1.gif")));
        ge.setRepeat(0);

        int count = gd.getFrameCount() ;
        int jump = getInt(count) ;
        LogKit.logf(">>>>%d>>>>>>%d>>>>>>>",count,jump);
        boolean heightWeight = false ;
        for (int i = 0; i < gd.getFrameCount(); i++) {
            BufferedImage frame = gd.getFrame(i);
            int width = frame.getWidth();
            int height = frame.getHeight();
            if(width < height){
                heightWeight = true ;
            }

            if(i % jump != 0){
                continue;
            }
            // 80%
            width = (int) (width * 0.5);
            height = (int) (height * 0.5);
            //
            BufferedImage rescaled = Scalr.resize(frame, heightWeight?Scalr.Mode.FIT_TO_HEIGHT:Scalr.Mode.FIT_TO_WIDTH, width, height);
            //
            int delay = gd.getDelay(i);
            //

            ge.setDelay(delay);
            ge.addFrame(rescaled);
        }

        ge.finish();
    }

    public static int getInt(int count){
        int i = 1 ;
        if(count > 50){
            i = 5 ;
        }else if(count > 30 ){
            i = 3 ;
        }else {
            i = 2 ;
        }
        return i ;
    }
}
